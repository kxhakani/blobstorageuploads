﻿using System;
using System.Linq;
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types
using Microsoft.Azure; // Namespace for CloudConfigurationManager
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace BlobStoragePoc
{
    class Program
    {

        public static bool result = true;
        public static string[] alldirs = new string[] { };

        [STAThread]
        static void Main(string[] args)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve a reference to a container.
            CloudBlobContainer container = blobClient.GetContainerReference("testing");

            // Create the container if it doesn't already exist.
            container.CreateIfNotExists();

            // Generate a SAS(Shared Access Policy) URI for the container.
            //Console.WriteLine("Container SAS URI: " + GetContainerSasUri(container));
            Console.WriteLine();

            try
            {
                //get all files in the main directory
                FileInfo[] f1 = new DirectoryInfo(@"C:\Uploads").EnumerateFiles()
                                         .Select(x =>
                                         {
                                             x.Refresh();
                                             return x;
                                         })
                                         .ToArray();

                FileInfo[] f2 = new FileInfo[] { }; 

                var allFiles = new FileInfo[] { };
                allFiles = f1;

                //get all subdirectories
                alldirs = Directory.GetDirectories(@"C:\Uploads");

                var sw = new Stopwatch();
                
                //get all files in the subdirectories 
                foreach (string item in alldirs)
                {
                    f2 = new DirectoryInfo(item).EnumerateFiles()
                                         .Select(x =>
                                         {
                                             x.Refresh();
                                             return x;
                                         })
                                         .ToArray();
                    if(f2.Length > 0)
                    {
                        allFiles = allFiles.Concat(f2).ToArray();
                    } 
                }
                try
                {
                    sw.Start();

                    //upload the new files in the storage account
                    UploadFilesToBlob(allFiles, container);

                    sw.Stop();
                }
                catch (StorageException e)
                {
                    //Console.WriteLine("Write operation failed for SAS " + sas);
                    Console.WriteLine("Additional error information: " + e.Message);
                    Console.WriteLine();
                }

                TimeSpan ts = sw.Elapsed;

                // Format and display the TimeSpan value.
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);

                Console.WriteLine();
                Console.WriteLine("Elapsed Time: " + elapsedTime);
                Console.WriteLine("Press ENTER to Exit ..... ");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine();
                Console.WriteLine("Press ENTER to Exit ..... ");
                Console.ReadLine();
            }
        }

        #region Delete directory after upload
        // Method to delete a directory if empty
        private static void processDirectory(string startLocation)
        {
            foreach (var directory in Directory.GetDirectories(startLocation))
            {
                processDirectory(directory);
                if (Directory.GetFiles(directory).Length == 0 &&
                    Directory.GetDirectories(directory).Length == 0)
                {
                    Directory.Delete(directory, false);
                }
            }
        }
        #endregion

        #region Shared Access Signature restriction based on IP address
        // Method that generates the shared access signature for the container and returns the signature URI.
        static string GetContainerSasUri(CloudBlobContainer container)
        {
            // Set the expiry time and permissions for the container.
            // In this case no start time is specified, so the shared access signature becomes valid immediately.
            SharedAccessBlobPolicy accessBlobPolicy = new SharedAccessBlobPolicy();
            accessBlobPolicy.SharedAccessExpiryTime = DateTimeOffset.UtcNow.AddYears(5);
            accessBlobPolicy.Permissions = SharedAccessBlobPermissions.List | SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.Read;
            IPAddressOrRange ipRange = new IPAddressOrRange("10.0.0.4", "168.63.129.16");

            // Generate the shared access signature on the container, setting the constraints directly on the signature.
            string sasContainerToken = container.GetSharedAccessSignature(accessBlobPolicy, null, null, ipRange);

            // Return the URI string for the container, including the SAS token.
            return container.Uri + sasContainerToken;
        }

        // Try performing container operations with the SAS provided.
        static void UseContainerSAS(string sas, string path)
        {
            // Return a reference to the container using the SAS URI.
            CloudBlobContainer container = new CloudBlobContainer(new Uri(sas));

            // Write operation: write a new blob to the container.
            try
            {
                string[] nameSplit = path.Split('\\');
                string name = nameSplit[nameSplit.Length - 1];

                // Retrieve reference to a blob named as the selected file name
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(name);

                // Create or overwrite the blob with contents from the local file.
                using (var fileStream = System.IO.File.OpenRead(path))
                {
                    if (blockBlob.Exists() == false)
                    {
                        blockBlob.UploadFromStream(fileStream);
                        Console.WriteLine("The file " + path + " was successfully uploaded!");
                        Console.WriteLine("Write operation succeeded for SAS " + sas);
                        Console.WriteLine();
                    }
                    else
                    {
                        result = false;
                        Console.WriteLine("The file " + path + " was not uploaded because it already exists!");
                        Console.WriteLine();
                    }
                }
            }
            catch (StorageException e)
            {
                Console.WriteLine("Write operation failed for SAS " + sas);
                Console.WriteLine("Additional error information: " + e.Message);
                Console.WriteLine();
            }
        }
        #endregion

        #region Upload files to blob
        private static void UploadFilesToBlob(FileInfo[] files, CloudBlobContainer container)
        {
            for (int i = 0; i < files.Length; i++)
            {
                try
                {
                    // Upload the files using the Shared Access Policy with IP Restriction 
                    // string name = fd.SafeFileName;
                    //UseContainerSAS(GetContainerSasUri(container), file);
                    #region //Upload file to blob 
                    // Write operation: write a new blob to the container.
                    try
                    {
                        //string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"C:\20180104\" + fileNames[j]);
                        string[] nameSplit = files[i].FullName.Split(new string[] { ":\\" }, StringSplitOptions.None);
                        string name = nameSplit[nameSplit.Length - 1];

                        // Retrieve reference to a blob named as the selected file name
                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(name);

                        // Create or overwrite the blob with contents from the local file.
                        using (var fileStream = System.IO.File.OpenRead(files[i].FullName))
                        {
                            if (blockBlob.Exists() == false)
                            {
                                blockBlob.UploadFromStream(fileStream);
                                Console.WriteLine("The file " + files[i].FullName + " was successfully uploaded!");
                                Console.WriteLine("Write operation succeeded for " + files[i].FullName);
                                Console.WriteLine();
                                result = true;
                            }
                            else
                            {
                                result = false;
                                Console.WriteLine("The file " + files[i].FullName + " was not uploaded because it already exists!");
                                Console.WriteLine();
                            }
                        }
                    }
                    catch (StorageException e)
                    {
                        //Console.WriteLine("Write operation failed for SAS " + sas);
                        Console.WriteLine("Additional error information: " + e.Message);
                        Console.WriteLine();
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }

                if (result == true)
                {
                    try
                    {
                        //delete file locally
                        files[i].Delete();
                        //call the method to delete a directory if empty
                        //processDirectory(@"C:\Uploads");

                        //for (int j = 0; j < alldirs.Length; j++)
                        //{
                        //    if (Directory.EnumerateDirectories(alldirs[j]).Count() == 0)
                        //        Directory.Delete(alldirs[j]);
                        //}

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Something went wrong and the file couldn't be deleted locally." + ex.Message);
                    }
                }
            }
        }
       #endregion 
    }
}
